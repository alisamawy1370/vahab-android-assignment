package com.vahabghadiri.core.utils

/**
 * this helper class and methods
 * help us to write better structures tests
 */
inline fun testCase(testCase: TestCase.() -> Unit) {
    TestCase().apply(testCase).apply {
        given?.invoke()
        whenever?.invoke() ?: throw NotImplementedError("test should have when")
        then?.invoke() ?: throw NotImplementedError("test should have then")
    }
}

class TestCase {

    var given: (() -> Unit)? = null
    var whenever: (() -> Unit)? = null
    var then: (() -> Unit)? = null

    fun given(block: () -> Unit) {
        given = block
    }

    fun <T> whenever(input: T, block: T.() -> Unit) {
        whenever = {
            block(input)
        }
    }

    fun then(block: () -> Unit) {
        then = block
    }
}