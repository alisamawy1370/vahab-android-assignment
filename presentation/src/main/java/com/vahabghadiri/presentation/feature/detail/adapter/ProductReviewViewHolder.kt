package com.vahabghadiri.presentation.feature.detail.adapter

import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem

/**
 * view holder for product review
 * @see ProductReviewsRvAdapter
 */
class ProductReviewViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val text = itemView.findViewById<TextView>(R.id.tvReviewText)
    private val rate = itemView.findViewById<RatingBar>(R.id.rate)

    fun bind(item: ProductReviewListItem) {
        text.text = item.text
        rate.rating = item.rate
    }
}