package com.vahabghadiri.presentation.feature.products

import com.vahabghadiri.domain.feature.products.model.ProductDomainModel
import com.vahabghadiri.presentation.feature.products.model.ProductListItem

fun ProductDomainModel.toListItem() = ProductListItem(
    id = id,
    name = name,
    description = description,
    price = "$price$currency",
    image = imgUrl
)