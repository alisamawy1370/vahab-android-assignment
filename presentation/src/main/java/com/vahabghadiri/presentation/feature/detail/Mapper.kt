package com.vahabghadiri.presentation.feature.detail

import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem

fun ProductReviewDomainModel.toListItem() = ProductReviewListItem(
    id, rate, text
)