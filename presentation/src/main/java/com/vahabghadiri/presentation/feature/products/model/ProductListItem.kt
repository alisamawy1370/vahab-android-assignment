package com.vahabghadiri.presentation.feature.products.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductListItem(
    val id: String,
    val name: String,
    val description: String,
    val price: String,
    val image: String
) : Parcelable