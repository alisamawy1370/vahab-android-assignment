package com.vahabghadiri.presentation.feature.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionInflater
import com.google.android.material.snackbar.Snackbar
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.base.BaseFragment
import com.vahabghadiri.presentation.databinding.FragmentDetailsBinding
import com.vahabghadiri.presentation.feature.detail.adapter.ProductReviewsRvAdapter
import com.vahabghadiri.presentation.feature.detail.model.ProductInfoModel
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem
import com.vahabghadiri.presentation.feature.review.AddReviewBottomSheetFragment
import com.vahabghadiri.presentation.feature.review.AddReviewCallBack
import com.vahabghadiri.presentation.utils.ImageLoader
import com.vahabghadiri.presentation.utils.extensions.createViewModel
import com.vahabghadiri.presentation.utils.extensions.observeNullSafe
import com.vahabghadiri.presentation.utils.extensions.setVisible

class DetailsFragment : BaseFragment(), AddReviewCallBack {

    private lateinit var viewBinding: FragmentDetailsBinding
    private lateinit var viewModel: DetailsViewModel
    private var recyclerAdapter: ProductReviewsRvAdapter? = null
    private var failureSnackBarView: Snackbar? = null
    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentDetailsBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupTransition()
    }

    private fun setupTransition() {
        sharedElementEnterTransition = TransitionInflater.from(context)
            .inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupTransitionViews()
        setupViewModel()
        viewModel.onViewCreated(args.product)
    }

    private fun setupTransitionViews() {
        ViewCompat.setTransitionName(viewBinding.ivProduct, args.product.id)
    }

    private fun setupViews() {
        // toolbar
        viewBinding.toolbar.ivBackArrow.apply {
            setVisible(true)
            setOnClickListener {
                findNavController().popBackStack()
            }
        }
        // init the recyclerView
        with(viewBinding.rvReviews) {
            recyclerAdapter = ProductReviewsRvAdapter(mutableListOf())
            this.layoutManager = LinearLayoutManager(requireContext())
            this.adapter = recyclerAdapter
        }

        // add review
        viewBinding.btnAddReview.setOnClickListener {
            showAddReviewBottomSheet()
        }
    }

    private fun setupViewModel() {
        viewModel = createViewModel(viewModelFactory) {
            updateInfoModelLiveData.observeNullSafe(viewLifecycleOwner) {
                updateProductInfoViews(it)
            }
            updateReviewsLiveData.observeNullSafe(viewLifecycleOwner) {
                updateProductReviewsList(it)
            }
            progressVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onProgressVisibilityChange(isVisible)
            }
            emptyViewVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onEmptyViewVisibilityChange(isVisible)
            }
            failureEventLiveData.observe(viewLifecycleOwner) { failureMessage ->
                handleFailureViews(failureMessage)
            }
        }
    }

    private fun updateProductReviewsList(newList: List<ProductReviewListItem>) {
        recyclerAdapter?.updateData(newList)
        viewBinding.rvReviews.post {
            viewBinding.rvReviews.smoothScrollToPosition(0)
        }
    }

    private fun onProgressVisibilityChange(isVisible: Boolean) {
        viewBinding.pgBottom.setVisible(isVisible)
    }

    private fun updateProductInfoViews(model: ProductInfoModel) {
        with(viewBinding) {
            toolbar.tvTitle.text = model.name
            tvProductName.text = model.name
            tvProductDescription.text = model.description
            ImageLoader.load(ivProduct, model.image)

            //priceView
            tvProductPrice.setVisible(model.price.isNotEmpty())
            tvProductPrice.text = model.price
        }
    }

    private fun onEmptyViewVisibilityChange(isVisible: Boolean) {
        viewBinding.tvEmptyReviews.setVisible(isVisible)
    }

    private fun handleFailureViews(failureMessage: String?) {

        fun showErrorSnackBar(message: String) {
            failureSnackBarView?.dismiss()
            failureSnackBarView = Snackbar.make(
                viewBinding.root,
                message,
                Snackbar.LENGTH_INDEFINITE
            ).apply {
                setAction(getString(R.string.retry)) {
                    viewModel.onReviewsRetryClick(args.product.id)
                }
            }
            failureSnackBarView?.show()
        }
        if (failureMessage.isNullOrBlank()) {
            failureSnackBarView?.dismiss()
        } else {
            showErrorSnackBar(failureMessage)
        }
    }

    private fun showAddReviewBottomSheet() {
        val currentSheet = childFragmentManager.findFragmentByTag(AddReviewBottomSheetFragment.TAG)
        if (currentSheet == null) {
            AddReviewBottomSheetFragment().show(
                childFragmentManager,
                AddReviewBottomSheetFragment.TAG
            )
        }
    }

    override fun onUserReviewAdd(rate: Float, text: String) {
        viewModel.onUserReviewAdded(rate, text, args.product.id)
    }
}