package com.vahabghadiri.presentation.di.module

import com.vahabghadiri.presentation.feature.detail.DetailsFragment
import com.vahabghadiri.presentation.feature.products.ProductsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * di module for injection in fragments so they will contribute for
 * android injection.
 */
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun productsFragment(): ProductsFragment

    @ContributesAndroidInjector
    abstract fun detailsFragment(): DetailsFragment
}