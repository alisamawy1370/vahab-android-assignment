package com.vahabghadiri.presentation.feature.products

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.products.usecase.GetProductListUseCase
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class ProductsViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val useCase: GetProductListUseCase = mock()
    private lateinit var globalDispatcher: GlobalDispatcher
    private lateinit var viewModel: ProductsViewModel

    @Before
    fun setup() {
        globalDispatcher = GlobalDispatcher(
            main = Dispatchers.Unconfined,
            io = Dispatchers.Unconfined,
            default = Dispatchers.Unconfined
        )
        viewModel = ProductsViewModel(
            useCase,
            globalDispatcher
        )
    }

    @Test
    fun `should get product list on page creation`() = testCase {
        whenever(viewModel) {
            onProductListViewCreated()
        }
        then {
            runBlocking {
                verify(useCase, times(1)).execute(any())
            }
        }
    }

    @Test
    fun `should update ui on api call succeed`() = testCase {
        val observer = mock<Observer<List<ProductListItem>>>()
        val argumentCaptor = argumentCaptor<List<ProductListItem>>()
        given {
            useCase.stub {
                onBlocking { execute(any()) } doReturn Result.Success(listOf())
            }
            viewModel.updateListLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onProductListViewCreated()
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
        }
    }

    @Test
    fun `should update ui on api call failure`() = testCase {
        val observer = mock<Observer<String?>>()
        val argumentCaptor = argumentCaptor<String>()
        given {
            useCase.stub {
                onBlocking { execute(any()) } doReturn Result.Failure("")
            }
            viewModel.failureEventLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onProductListViewCreated()
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
        }
    }

    @Test
    fun `should get product list on user retry`() = testCase {
        whenever(viewModel) {
            onProductListRetryClick()
        }
        then {
            runBlocking {
                verify(useCase, times(1)).execute(any())
            }
        }
    }
}