package com.vahabghadiri.domain.feature.review.usecase

import com.vahabghadiri.domain.feature.review.worker.SubmitReviewWorkScheduler
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import com.vahabghadiri.domain.model.usecase.UseCaseParam
import javax.inject.Inject

/**
 * UseCase to submit all pending reviews
 * @see UseCase
 * @see Result
 */
class SubmitPendingReviewUseCase @Inject constructor(
    private val submitReviewWorkScheduler: SubmitReviewWorkScheduler
) : UseCase<Unit, UseCaseParam> {

    override suspend fun execute(params: UseCaseParam): Result<Unit> {
        submitReviewWorkScheduler.scheduleSubmitPendingReviews()
        return Result.Success(Unit)
    }
}