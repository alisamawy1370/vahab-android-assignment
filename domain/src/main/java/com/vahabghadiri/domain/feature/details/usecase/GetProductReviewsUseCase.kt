package com.vahabghadiri.domain.feature.details.usecase

import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.feature.details.repository.ProductReviewRepository
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to get product reviews by id
 * @see UseCase
 * @see Result
 */
class GetProductReviewsUseCase @Inject constructor(
    private val productReviewRepository: ProductReviewRepository
) : UseCase<ProductReviewsDomainModel, GetProductReviewsParam> {

    override suspend fun execute(params: GetProductReviewsParam): Result<ProductReviewsDomainModel> {
        return productReviewRepository.getProductReviews(params.productId)
    }
}