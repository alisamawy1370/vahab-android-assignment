package com.vahabghadiri.domain.feature.details.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using get product reviews UseCase
 */
data class GetProductReviewsParam(val productId: String) : UseCaseParam()