package com.vahabghadiri.domain.feature.details.model

data class ProductReviewDomainModel(
    val id: String,
    val rate: Float,
    val text: String
)