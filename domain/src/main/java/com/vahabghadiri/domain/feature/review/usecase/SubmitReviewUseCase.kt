package com.vahabghadiri.domain.feature.review.usecase

import com.vahabghadiri.domain.feature.details.repository.ProductReviewRepository
import com.vahabghadiri.domain.feature.review.worker.SubmitReviewWorkScheduler
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to submit review
 * @see UseCase
 * @see Result
 */
class SubmitReviewUseCase @Inject constructor(
    private val productReviewRepository: ProductReviewRepository,
    private val submitReviewWorkScheduler: SubmitReviewWorkScheduler
) : UseCase<Unit, SubmitReviewParam> {

    override suspend fun execute(params: SubmitReviewParam): Result<Unit> {
        productReviewRepository.persistReview(
            params.productId,
            params.rate,
            params.text
        )
        submitReviewWorkScheduler.scheduleSubmitReview(
            params.productId,
            params.rate,
            params.text
        )
        return Result.Success(Unit)
    }

    companion object {

        /**
         * user should have only one review which is not sent
         */
        const val USER_PEND_REVIEW_ID = "11111"
    }
}