package com.vahabghadiri.domain.feature.review.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using get product reviews UseCase
 */
data class SubmitReviewParam(
    val productId: String,
    val text: String,
    val rate: Float
) : UseCaseParam()