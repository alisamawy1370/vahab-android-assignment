package com.vahabghadiri.data.di.module

import com.vahabghadiri.data.di.workmanager.ChildWorkerFactory
import com.vahabghadiri.data.di.workmanager.WorkerKey
import com.vahabghadiri.data.feature.review.worker.SubmitPendingReviewsWorker
import com.vahabghadiri.data.feature.review.worker.SubmitReviewWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface WorkerMultiBindModule {

    @Binds
    @IntoMap
    @WorkerKey(SubmitReviewWorker::class)
    fun bindSubmitReviewWorker(
        factory: SubmitReviewWorker.Factory
    ): ChildWorkerFactory

    @Binds
    @IntoMap
    @WorkerKey(SubmitPendingReviewsWorker::class)
    fun bindSubmitPendingReviewsWorker(
        factory: SubmitPendingReviewsWorker.Factory
    ): ChildWorkerFactory
}