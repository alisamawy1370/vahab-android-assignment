package com.vahabghadiri.data.di.module

import com.vahabghadiri.data.di.qualifer.Review
import com.vahabghadiri.data.feature.details.GetProductDetailApiService
import com.vahabghadiri.data.feature.products.remote.GetProductsApiService
import com.vahabghadiri.data.feature.review.remote.ProductReviewApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * will provide network api services
 * which will be used in data module and remote data sources.
 */
@Module
open class RemoteApiModule {

    @Singleton
    @Provides
    open fun provideProductsService(retrofit: Retrofit): GetProductsApiService =
        retrofit.create(GetProductsApiService::class.java)

    @Singleton
    @Provides
    open fun provideProductDetailApiService(retrofit: Retrofit): GetProductDetailApiService =
        retrofit.create(GetProductDetailApiService::class.java)

    @Singleton
    @Provides
    open fun provideProductReviewApiService(@Review retrofit: Retrofit): ProductReviewApiService =
        retrofit.create(ProductReviewApiService::class.java)
}