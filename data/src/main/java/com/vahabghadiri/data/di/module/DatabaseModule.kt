package com.vahabghadiri.data.di.module

import android.content.Context
import androidx.room.Room
import com.vahabghadiri.data.db.AppDatabase
import com.vahabghadiri.data.feature.review.local.PendingReviewDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * will provide Database and it's DAO objects for data layer
 */
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "adidas-app-db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun providePendingReviewDao(appDatabase: AppDatabase): PendingReviewDao {
        return appDatabase.pendingReviewDao()
    }
}