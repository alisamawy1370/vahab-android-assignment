package com.vahabghadiri.data.feature.products.remote

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.feature.products.model.ProductDataModel
import com.vahabghadiri.data.feature.products.toDataModel
import com.vahabghadiri.data.util.ApiResult
import com.vahabghadiri.data.util.callAwait
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductsListRemoteDataSource @Inject constructor(
    private val getProductsApiService: GetProductsApiService,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun getProducts(): ApiResult<List<ProductDataModel>> {
        return withContext(globalDispatcher.io) {
            getProductsApiService.getProductsList().callAwait { list ->
                list.map { productResponseDto ->
                    productResponseDto.toDataModel()
                }
            }
        }
    }
}