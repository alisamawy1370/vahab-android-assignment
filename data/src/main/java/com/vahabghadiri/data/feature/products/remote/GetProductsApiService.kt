package com.vahabghadiri.data.feature.products.remote

import com.vahabghadiri.data.feature.products.remote.model.ProductResponseDto
import retrofit2.Call
import retrofit2.http.GET

interface GetProductsApiService {

    @GET("product")
    fun getProductsList(): Call<List<ProductResponseDto>>
}