package com.vahabghadiri.data.feature.products

import com.vahabghadiri.data.feature.products.model.ProductDataModel
import com.vahabghadiri.data.feature.products.remote.model.ProductResponseDto
import com.vahabghadiri.domain.feature.products.model.ProductDomainModel

/**
 * mapper file for each layer could be mandatory
 * its kind of mapper hell but each layer should have it's
 * own model
 */

fun ProductResponseDto.toDataModel() = ProductDataModel(
    id, name, description, price, currency, imgUrl
)

fun ProductDataModel.toDomainModel() = ProductDomainModel(
    id, name, description, price, currency, imgUrl
)