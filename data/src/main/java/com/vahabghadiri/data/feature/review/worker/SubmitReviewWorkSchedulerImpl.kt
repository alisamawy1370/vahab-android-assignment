package com.vahabghadiri.data.feature.review.worker

import android.content.Context
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.vahabghadiri.domain.feature.review.worker.SubmitReviewWorkScheduler
import javax.inject.Inject

class SubmitReviewWorkSchedulerImpl @Inject constructor(
    private val context: Context
) : SubmitReviewWorkScheduler {

    override fun scheduleSubmitReview(productId: String, rate: Float, text: String) {
        val request: WorkRequest = OneTimeWorkRequestBuilder<SubmitReviewWorker>()
            .setInputData(
                SubmitReviewWorker.toInputData(
                    productId, text, rate
                )
            )
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .build()
        WorkManager.getInstance(context).enqueue(request)
    }

    override fun scheduleSubmitPendingReviews() {
        val request: WorkRequest = OneTimeWorkRequestBuilder<SubmitPendingReviewsWorker>()
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .build()
        WorkManager.getInstance(context).enqueue(request)
    }
}