package com.vahabghadiri.data.feature.review.local

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.feature.details.GetProductDetailApiService
import com.vahabghadiri.data.feature.review.model.ProductReviewsDataModel
import com.vahabghadiri.data.feature.review.model.SubmitReviewDataModel
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewRequestDto
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewResponseDto
import com.vahabghadiri.data.feature.review.toDataModel
import com.vahabghadiri.data.feature.review.toLocalEntity
import com.vahabghadiri.data.util.ApiResult
import com.vahabghadiri.data.util.callAwait
import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductReviewLocalDataSource @Inject constructor(
    private val pendingReviewDao: PendingReviewDao,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun persistReview(reviewDataModel: SubmitReviewDataModel) {
        return withContext(globalDispatcher.io) {
            pendingReviewDao.saveObject(reviewDataModel.toLocalEntity())
        }
    }

    @Throws
    suspend fun getPersistedReview(productId: String): SubmitReviewDataModel? {
        return withContext(globalDispatcher.io) {
            pendingReviewDao.getObject(productId)?.toDataModel()
        }
    }

    @Throws
    suspend fun removePersistedReview(productId: String) {
        return withContext(globalDispatcher.io) {
            pendingReviewDao.delete(productId)
        }
    }

    @Throws
    suspend fun getAllPersistedReviews(): List<SubmitReviewDataModel> {
        return withContext(globalDispatcher.io) {
            pendingReviewDao.getAllObjects().map {
                it.toDataModel()
            }
        }
    }
}