package com.vahabghadiri.data.feature.review.model

data class ProductReviewsDataModel(
    val id: String,
    val reviews: List<ProductReviewDataModel>
)