package com.vahabghadiri.data.feature.review.model

data class SubmitReviewDataModel(
    val productId: String,
    val text: String,
    val rate: Float
)