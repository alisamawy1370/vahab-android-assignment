package com.vahabghadiri.data.feature.review.model

data class ProductReviewDataModel(
    val id: String,
    val rate: Float,
    val text: String
)