package com.vahabghadiri.data.feature.review.remote

import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewRequestDto
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewResponseDto
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface ProductReviewApiService {

    @POST("reviews/{id}")
    fun submitReview(
        @Path("id") productId: String,
        @Body request: SubmitReviewRequestDto
    ): Call<SubmitReviewResponseDto>
}