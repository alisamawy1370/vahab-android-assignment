package com.vahabghadiri.data.feature.review.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vahabghadiri.data.feature.review.local.model.ReviewLocalEntity

@Dao
interface PendingReviewDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveObject(reviewLocalEntity: ReviewLocalEntity)

    @Query("select * from pending_review where id=:id")
    suspend fun getObject(id: String): ReviewLocalEntity?

    @Query("DELETE FROM pending_review where id=:id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM pending_review")
    suspend fun getAllObjects(): List<ReviewLocalEntity>
}