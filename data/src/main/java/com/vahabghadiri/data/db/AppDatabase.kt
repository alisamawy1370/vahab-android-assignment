package com.vahabghadiri.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vahabghadiri.data.feature.review.local.PendingReviewDao
import com.vahabghadiri.data.feature.review.local.model.ReviewLocalEntity

@Database(entities = [ReviewLocalEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pendingReviewDao(): PendingReviewDao
}